//
//  FirstViewController.swift
//  SP101
//
//  Created by formando on 22/02/2018.
//  Copyright © 2018 ipleiria. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var labelChangeOnButtonContinue: UILabel!
    
    @IBAction func buttonContinuePressed(_ sender: UIButton) {
        labelChangeOnButtonContinue.text = "Insert your location"
    }
    
    @IBOutlet weak var firstName: UITextField!
    @IBAction func SendData(_ sender: UIButton) {
        
    }
}

